-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2016 at 07:23 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(12) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `birthday`, `created`, `updated`, `deleted`) VALUES
(1, '1994-11-25', NULL, NULL, NULL),
(2, '1991-11-25', NULL, NULL, NULL),
(3, '2015-12-12', NULL, NULL, '2016-04-08');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(15) NOT NULL,
  `title` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Database Programming  1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Object Oriented Programming', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-22'),
(7, 'C++', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'OOP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dristric`
--

CREATE TABLE `dristric` (
  `id` int(12) NOT NULL,
  `dristric` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dristric`
--

INSERT INTO `dristric` (`id`, `dristric`, `created`, `updated`, `deleted`) VALUES
(1, 'Noakhali', NULL, NULL, NULL),
(2, 'Dhaka', NULL, NULL, NULL),
(3, 'Comilla', NULL, NULL, NULL),
(4, 'Noakhali', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `created`, `updated`, `deleted`) VALUES
(1, 'shahabuddin_p@yahoo.com', NULL, NULL, NULL),
(2, 'abc@gmail.com', NULL, NULL, '2016-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(12) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender`, `created`, `updated`, `deleted`) VALUES
(1, 'Male', NULL, NULL, NULL),
(2, 'Female', NULL, NULL, NULL),
(3, 'Male', NULL, NULL, '2016-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(15) NOT NULL,
  `hobby` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobby`, `created`, `updated`, `deleted`) VALUES
(1, 'Cricket,Football,Codding', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Cricket,Football,Codding,Gardening', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-06'),
(0, 'Football,Gardening', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(0, 'Football,Gardening', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby2`
--

CREATE TABLE `hobby2` (
  `id` int(15) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby2`
--

INSERT INTO `hobby2` (`id`, `hobby`, `created`, `updated`, `deleted`) VALUES
(1, 'Cricket', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_upload_pda`
--

CREATE TABLE `image_upload_pda` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` int(12) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Nokia Lumia', NULL, NULL, NULL),
(2, 'Nokia 1200', NULL, NULL, '2016-04-08'),
(3, 'Walton', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id`, `name`, `picture`, `created`, `updated`, `deleted`, `active`) VALUES
(1, 'Md. Shahab uddin', '14601311961.png', NULL, NULL, NULL, 0),
(2, 'Karim', '14601324465.jpg', NULL, NULL, NULL, 0),
(3, 'Shuvo', '14601329764.jpg', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `textarea`
--

CREATE TABLE `textarea` (
  `id` int(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `textarea`
--

INSERT INTO `textarea` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Aspire Tech Services & Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-25'),
(2, 'Bangladesh University.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-25'),
(3, 'BASIS INSTITUTE OF TECHNOLOGY & MANAGEMENT', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Kalikapur High School', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_upload_pda`
--
ALTER TABLE `image_upload_pda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `image_upload_pda`
--
ALTER TABLE `image_upload_pda`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
