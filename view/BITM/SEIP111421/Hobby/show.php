<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Combine Project</title>

        <link href="../../../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../../css/font-awesome.min.css" rel="stylesheet">
        <link href="../../../../style.css" rel="stylesheet">
        <link href="../../../../css/responsive.css" rel="stylesheet">


        <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="header_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div class="logo_area">
                            <img class="logo_img" src="../../../../images/logo.png" alt="logo" title="Upashom Hospital .">
                            <p class="h_name">BASIS</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3">
                        <div class="top_menu">
                            <ul>
                                <li><a href="">Home</a></li>
                                <li><a href="">About</a></li>
                                <li><a href="">Career</a></li>
                                <li><a href="">FAQ</a></li>
                                <li><a href="">Contact</a></li>
                            </ul>
                            
<!--                            <p class="call1"><i class="icon-phone"><span>10666</span></i></p>
                            <p class="call"> +880 01914 001234 </p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="main_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main_m">       
                            <nav class="navbar navbar-inverse">
                                <div class="container-fluid">
                                    <div class="navbar-header"></div>
                                        <ul class="nav navbar-nav">
                                            <li class=""><a href="#"><i class="icon-home"></i></a></li>
                                            <li class=""><a href="view/BITM/SEIP111421/Mobile/index.php">Mobile</a></li>
                                            <li class=""><a href="../Mobile/index.php">Mobile</a></li>
                                            <li><a href="../Book/index.php">Book</a></li>
                                            <li><a href="../Birthday/index.php">Birthday</a></li>
                                            <li><a href="../textarea/index.php">Textarea</a></li>
                                            <li><a href="../Email/index.php">Email</a></li>
                                            <li><a href="../Picture/index.php">Profile Picture</a></li>
                                            <li><a href="../Gender/index.php">Gender</a></li>
                                            <li><a href="index.php">Hobby</a></li>
                                            <li><a href="../City/index.php">Distric</a></li>
                                        </ul>
                                    </div>
                            </nav>
                        </div>
<!--
                        <div class="search">
                            <input type="text"><i class="icon-search"></i>
                        </div>
-->
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content">
                  <?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Hobby\Hobbies;
$id=$_GET['id'];
$hobby =new Hobbies();
$onehobby=$hobby->show($id);
?>
<html>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Hobby</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onehobby['id']?></td>
            <td>
                <?php 
                if(isset($onehobby['hobby']) && !empty($onehobby['hobby'])){
                    echo $onehobby['hobby'];
                }else{
                    echo "Not Available";
                }
                ?>
            </td>
            <td><?php echo $onehobby['created']?></td>
            <td>
                <a href="edit.php?id=<?php echo $onehobby['id'];?>">Edit</a> | 
                <a href="delete.php?id=<?php echo $onehobby['id'];?>">Delete</a> 
            </td>
        </tr>
    </table>
</html>

        </div>


       
    </body>
</html>





