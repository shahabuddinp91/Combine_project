<?php
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Textarea\Textarea;

$update = new Textarea();
$update->prepare($_POST);
$update->update();