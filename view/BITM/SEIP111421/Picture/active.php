<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Picture\Picture;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$picture = new Picture();
$picture ->prepare($_GET);
$picture ->updateActivated();
$onePicture = $picture->active();

?>
<img src="<?php echo "../../../../images/".$onePicture['picture'] ?>" width="200" height="140">